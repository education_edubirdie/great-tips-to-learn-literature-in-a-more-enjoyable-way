# Great Tips to Learn Literature in a More Enjoyable Way

Learning English literature is not easy, and many learners have to study it at some time. With so many [writing](https://advice.writing.utoronto.ca/general/general-advice/) assignments, you might feel overwhelmed even to think where to begin. Luckily, you can use these great tips to make literature more enjoyable. 

## Always Read With a Paper and A Pen

While reading any literary work, ensure you have a book and pen to take notes. Be sure to ask questions and highlight important messages while reading. While in class, always note down the ideas discussed and passages analyzed since they are more likely to come in exams. Furthermore, ensure that you keep track of more important details like 'title' and 'author,' 'character names,' 'plot,' 'themes,' among others. Also, read essay samples to get some inspiration and ideas for your assignment.

## Read Actively

Whether you are reading literary work examples or novels, keep your brain awake. Most importantly, ask yourself questions while reading. Take breaks after one or two chapters and check your understanding of the book. Think about:
- What is happening?
- What will happen next? 
- Who is speaking, and what things do you know about the speaker?
- What inspires the narrator, character, or author? Particularly if it is a non-fiction text.

##Start Early

It is a bad habit for learners to wait until the last night before exams. Especially for English literature since you may probably be asked analytical and content questions. You need time to formalize yourself with some of the complex materials. Knowing and summarizing a plot or character might not be all you need to do. 
So, ensure you prepare and start early. Start by reading interesting papers about [Romeo and Juliet](https://edubirdie.com/examples/romeo-and-juliet/) published on the web. Reading paper samples on various topics surrounding this play will help you get ideas you can use on your assignment. 

## Identify Problem Spots

These are areas where you do not understand the narrative, something unusual happened, or the language confuses you. Usually, these are good areas to analyze in papers so take note of them. You can discuss these points with other students. Also, ask queries on segments that you do not understand.

## Review Notes More Often

Ensure you read your class notes regularly and even transfer them to Google Docs, Word, Evernote, or OneNote. Try developing an outline for the content and organizing them. Transferring notes on your computer ensures they are more searchable, making it easy when you need to study.

## Conclusion

These tips will make [learning](https://www.thoughtco.com/common-book-themes-1857647) literary work more enjoyable. It may seem like too much work just for one text. Remember to excel; you need to put more time, effort, and energy into reading a text well.
